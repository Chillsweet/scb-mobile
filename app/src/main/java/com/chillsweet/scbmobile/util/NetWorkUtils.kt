package com.chillsweet.scbmobile.util

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import com.chillsweet.scbmobile.custom.DialogAlertCustom


/**
 * Created by chillsweet on 12/6/2018 AD
 */
object NetWorkUtils {

    fun isNetworkAvailable(activity: Activity, isShowDialog: Boolean): Boolean {
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return when (networkInfo != null && networkInfo.isAvailable) {
            true -> true
            else ->
                when (isShowDialog) {
                    true -> {
                        DialogAlertCustom(activity).alertNetWorkNotAvailable()
                        false
                    }
                    else -> false
                }
        }
    }

    fun isWifiAvailable(context: Context): Boolean {
        val connectivityManager = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isAvailable && networkInfo.type == ConnectivityManager.TYPE_WIFI
    }

}