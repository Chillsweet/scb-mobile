package com.chillsweet.scbmobile.data.sqlite

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@SuppressWarnings("WeakerAccess")
class SqliteTable {

    companion object {

        const val TABLE_MOBILE = "table_mobile"
        const val MOBILE_ID = "mobile_id"
        const val MOBILE_RATING = "mobile_rating"
        const val MOBILE_BRAND = "mobile_brand"
        const val MOBILE_NAME = "mobile_name"
        const val MOBILE_PRICE = "mobile_price"
        const val MOBILE_THUMB_IMAGE_URL = "mobile_thumb_image_url"
        const val MOBILE_DESCRIPTION = "mobile_description"

        val DB_MOBILE = (
                "CREATE TABLE " + TABLE_MOBILE
                        + "("
                        + MOBILE_ID + " INTEGER PRIMARY KEY, "
                        + MOBILE_RATING + " DOUBLE, "
                        + MOBILE_BRAND + " TEXT, "
                        + MOBILE_NAME + " TEXT, "
                        + MOBILE_PRICE + " DOUBLE, "
                        + MOBILE_THUMB_IMAGE_URL + " TEXT, "
                        + MOBILE_DESCRIPTION + " TEXT "
                        + ")"
                )
    }

}