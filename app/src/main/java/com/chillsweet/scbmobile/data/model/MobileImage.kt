package com.chillsweet.scbmobile.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Parcelize
data class MobileImage(val id: Int,
                        val mobile_id: Int,
                        val url: String?) : Parcelable