package com.chillsweet.scbmobile.data.remote

import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.data.model.MobileImage
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by chillsweet on 30/6/2018 AD
 */
interface Services {

    @GET("mobiles/")
    fun callMobileList(): Single<Response<ArrayList<Mobile>>>

    @GET("mobiles/{mobile_id}/images")
    fun callMobileImageList(@Path("mobile_id") mobileId: Int): Single<Response<ArrayList<MobileImage>>>

}