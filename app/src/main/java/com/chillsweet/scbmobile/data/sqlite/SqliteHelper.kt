package com.chillsweet.scbmobile.data.sqlite

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.ContentValues
import android.content.Context
import android.util.Log
import com.chillsweet.scbmobile.data.model.Mobile
import javax.inject.Inject


/**
 * Created by chillsweet on 30/6/2018 AD
 */
class SqliteHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {

        const val DATABASE_NAME = "scb_mobile_database"
        const val DATABASE_VERSION = 1
        private var TAG = SqliteHelper::class.java.simpleName
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SqliteTable.DB_MOBILE)
    }

    override fun onUpgrade(db: SQLiteDatabase,
                           oldVersion: Int,
                           newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + SqliteTable.DB_MOBILE)
        onCreate(db)
    }

    fun insertData(table: String,
                   values: ContentValues): Boolean {
        val db = writableDatabase
        val result = db.insert(table, null, values)
        return if (result == -1L) {
            Log.d(TAG, "failed to save data!")
            false
        }
        else {
            Log.d(TAG, "save data successful")
            true
        }
    }

    fun updateData(table: String,
                   values: ContentValues,
                   id: Int): Int {
        val db = this.writableDatabase
        return db.update(table, values,
                SqliteTable.MOBILE_ID + " =? ",
                arrayOf(id.toString()))
    }

    fun isMobileFavoriteExist(id: Int): Boolean {
        val isExist: Boolean
        val db = this.readableDatabase
        val cursor = db.rawQuery("select * from ${SqliteTable.TABLE_MOBILE} where ${SqliteTable.MOBILE_ID} = $id", null)
        isExist = cursor.moveToFirst()
        cursor.close()
        db.close()
        return isExist
    }

    fun getMobileFavorite(): ArrayList<Mobile>? {
        val mobileList = ArrayList<Mobile>()
        val db = this.readableDatabase
        val cursor = db.rawQuery("select * from ${SqliteTable.TABLE_MOBILE}", null)
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                val mobile = Mobile(
                        id = cursor.getInt(0),
                        rating = cursor.getFloat(1),
                        brand = cursor.getString(2),
                        name = cursor.getString(3),
                        price = cursor.getFloat(4),
                        url = cursor.getString(5),
                        description = cursor.getString(6)
                )
                mobileList.add(mobile)
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()

        return if (mobileList.size > 0)
            mobileList
        else
            null
    }

    fun deleteMobileFavorite(id: Int): Boolean {
        val db = this.writableDatabase
        return db.delete(SqliteTable.TABLE_MOBILE, SqliteTable.MOBILE_ID + " = $id", null) > 0
    }

}