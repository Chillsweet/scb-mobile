package com.chillsweet.scbmobile.data.sqlite

import android.content.ContentValues
import android.content.Context
import com.chillsweet.scbmobile.data.model.Mobile
import javax.inject.Inject


/**
 * Created by chillsweet on 30/6/2018 AD
 */
@SuppressWarnings("WeakerAccess")
class SqliteController @Inject
constructor(context: Context) {

    private var sqliteHelper: SqliteHelper = SqliteHelper(context)

    fun saveMobileFavorite(mobile: Mobile): Boolean {
        val values = ContentValues()
        values.put(SqliteTable.MOBILE_ID, mobile.id)
        values.put(SqliteTable.MOBILE_RATING, mobile.rating)
        values.put(SqliteTable.MOBILE_BRAND, mobile.brand)
        values.put(SqliteTable.MOBILE_NAME, mobile.name)
        values.put(SqliteTable.MOBILE_PRICE, mobile.price)
        values.put(SqliteTable.MOBILE_THUMB_IMAGE_URL, mobile.url)
        values.put(SqliteTable.MOBILE_DESCRIPTION, mobile.description)
        return sqliteHelper.insertData(SqliteTable.TABLE_MOBILE, values)
    }

    fun updateMobileFavorite(mobile: Mobile): Int {
        val values = ContentValues()
        values.put(SqliteTable.MOBILE_RATING, mobile.rating)
        values.put(SqliteTable.MOBILE_BRAND, mobile.brand)
        values.put(SqliteTable.MOBILE_NAME, mobile.name)
        values.put(SqliteTable.MOBILE_PRICE, mobile.price)
        values.put(SqliteTable.MOBILE_THUMB_IMAGE_URL, mobile.url)
        values.put(SqliteTable.MOBILE_DESCRIPTION, mobile.description)
        return sqliteHelper.updateData(SqliteTable.TABLE_MOBILE, values, mobile.id)
    }

    fun isMobileFavoriteExist(mobile: Mobile) = sqliteHelper.isMobileFavoriteExist(mobile.id)

    fun getMobileFavorite(): ArrayList<Mobile>? = sqliteHelper.getMobileFavorite()

    fun deleteMobileFavorite(mobile: Mobile) {
        sqliteHelper.deleteMobileFavorite(mobile.id)
    }

}