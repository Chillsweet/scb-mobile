package com.chillsweet.scbmobile.data.remote

import android.app.Activity
import android.widget.Toast
import com.chillsweet.scbmobile.custom.DialogAlertCustom
import com.chillsweet.scbmobile.data.DataManager
import java.net.SocketTimeoutException

/**
 * Created by chillsweet on 7/5/2017 AD.
 */
object ServiceResult {

    val Success = 200..299

    const val Success_200 = 200

    const val Success_201 = 201

    const val Success_202 = 202

    const val Success_204 = 204

    val Error = 400..500

    const val Error_400 = 400

    const val Error_401 = 401

    const val Error_403 = 403

    const val Error_404 = 404

    const val Error_406 = 406

    const val Error_423 = 423

    const val Error_500 = 500

    fun onAlertError(activity: Activity, responseCode: Int, mDataManager: DataManager) {
        DialogAlertCustom(activity).alertRequestNotSuccess(responseCode, "")
    }

    fun onThrowableException(activity: Activity, throwable: Throwable) {
        when (throwable) {
            is SocketTimeoutException -> {
                Toast.makeText(activity, "Time Out!!", Toast.LENGTH_LONG).show()
                activity.finish()
            }
        }
    }

}