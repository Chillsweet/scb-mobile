package com.chillsweet.scbmobile.data

import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.data.model.MobileImage
import com.chillsweet.scbmobile.data.remote.Services
import com.chillsweet.scbmobile.data.sqlite.SqliteController
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Singleton
class DataManager @Inject constructor(val mServices: Services,
                                      val mSqlite: SqliteController) {


    /**
     * API
     */

    fun callMobileList(): Single<Response<ArrayList<Mobile>>> {
        return mServices.callMobileList()
    }

    fun callMobileImageList(mobileId: Int): Single<Response<ArrayList<MobileImage>>> {
        return mServices.callMobileImageList(mobileId)
    }

    /**
     * Sqlite
     */

    fun saveMobileFavorite(mobile: Mobile) {
        mSqlite.saveMobileFavorite(mobile)
    }

    fun updateMobileFavorite(mobile: Mobile) {
        mSqlite.updateMobileFavorite(mobile)
    }

    fun getMobileFavorite(): ArrayList<Mobile>? {
        return mSqlite.getMobileFavorite()
    }

    fun deleteMobileFavorite(mobile: Mobile) {
        mSqlite.deleteMobileFavorite(mobile)
    }

}