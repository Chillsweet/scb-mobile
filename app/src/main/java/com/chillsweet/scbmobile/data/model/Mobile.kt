package com.chillsweet.scbmobile.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Parcelize
data class Mobile(val id: Int,
                  val rating: Float?,
                  val brand: String?,
                  val name: String?,
                  val price: Float?,
                  @SerializedName("thumbImageURL") val url: String?,
                  val description: String?) : Parcelable {

    companion object {

        var lowPrice: Comparator<Mobile> = Comparator { m1, m2 ->
            val lowPrice1 = m1.price
            val lowPrice2 = m2.price

            /*For ascending order*/
            if (lowPrice1 == null && lowPrice2 == null) 0
            else if (lowPrice1 == null) lowPrice2!!.toInt()
            else if (lowPrice2 == null) lowPrice1.toInt()
            else lowPrice1.compareTo(lowPrice2)
        }

        var highPrice: Comparator<Mobile> = Comparator { m1, m2 ->
            val lowPrice1 = m1.price
            val lowPrice2 = m2.price

            /*For descending order*/
            if (lowPrice1 == null && lowPrice2 == null) 0
            else if (lowPrice1 == null) lowPrice2!!.toInt()
            else if (lowPrice2 == null) lowPrice1.toInt()
            else lowPrice2.compareTo(lowPrice1)
        }

        var rating51: Comparator<Mobile> = Comparator { r1, r2 ->
            val rating1 = r1.rating
            val rating2 = r2.rating

            /*For descending order*/
            if (rating1 == null && rating2 == null) 0
            else if (rating1 == null) rating2!!.toInt()
            else if (rating2 == null) rating1.toInt()
            else rating2.compareTo(rating1)
        }
    }
}