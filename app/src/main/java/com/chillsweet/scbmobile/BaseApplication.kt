package com.chillsweet.scbmobile

import android.app.Application
import android.content.Context
import com.chillsweet.scbmobile.injection.component.ApplicationComponent
import com.chillsweet.scbmobile.injection.component.DaggerApplicationComponent
import com.chillsweet.scbmobile.injection.module.ApplicationModule
import timber.log.Timber

/**
 * Created by chillsweet on 30/6/2018 AD
 */
class BaseApplication : Application() {

    internal var mApplicationComponent: ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    var component: ApplicationComponent
        get() {
            if (mApplicationComponent == null) {
                mApplicationComponent = DaggerApplicationComponent.builder()
                        .applicationModule(ApplicationModule(this))
                        .build()
            }
            return mApplicationComponent as ApplicationComponent
        }
        set(applicationComponent) {
            mApplicationComponent = applicationComponent
        }

    companion object {
        operator fun get(context: Context): BaseApplication = context.applicationContext as BaseApplication
    }
}