package com.chillsweet.scbmobile.extension

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.chillsweet.scbmobile.R


/**
 * Created by chillsweet on 11/6/2017 AD.
 */
fun ImageView.setImageUrl(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.preload))
            .transition(withCrossFade())
            .into(this)
}

fun ImageView.setImageCenterCrop(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions().centerCrop().error(R.drawable.preload))
            .transition(withCrossFade())
            .into(this)
}

fun ImageView.setImageCenterCrop(@DrawableRes urlId: Int) {
    Glide.with(context)
            .load(urlId)
            .apply(RequestOptions().centerCrop())
            .transition(withCrossFade())
            .into(this)
}

fun ImageView.setImageFitCenter(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.preload))
            .transition(withCrossFade())
            .into(this)
}

fun ImageView.setImageCenterInside(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions().centerInside().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.preload))
            .transition(withCrossFade())
            .into(this)
}

fun ImageView.setImageCircle(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions.circleCropTransform().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.preload).dontAnimate())
            .into(this)
}