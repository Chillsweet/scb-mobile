package com.chillsweet.scbmobile.custom

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.util.Constance
import kotlinx.android.synthetic.main.dialog_filter_custom.view.*

/**
 * Created by chillsweet on 9/18/2017 AD.
 */
class DialogFilterCustom(val context: Context) {

    val dialog = Dialog(context)
    val view: View = LayoutInflater.from(context)
            .inflate(R.layout.dialog_filter_custom, null, false)
    private lateinit var clickListener: ClickListener

    init {
        setDialog()
    }

    fun setDialog() {
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.setContentView(view)
        dialog.setCancelable(true)
    }

    fun showDialog(checkedId: Int,
                   clickListener: ClickListener) {
        setView(checkedId)
        setListener()
        setClickListener(clickListener)
        dialog.show()
    }

    fun setView(checkedId: Int) {
        if (checkedId != 0)
            view.radioGroup.check(checkedId)
    }

    fun setListener() {
        view.apply {
            radioGroup.setOnCheckedChangeListener { radioGroup, checkedId ->
                when (checkedId) {
                    R.id.radio1 -> clickListener.onFilterClicked(checkedId, Constance.FILTER_1)
                    R.id.radio2 -> clickListener.onFilterClicked(checkedId, Constance.FILTER_2)
                    R.id.radio3 -> clickListener.onFilterClicked(checkedId, Constance.FILTER_3)
                }
                dialog.dismiss()
            }
        }
    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    interface ClickListener {
        fun onFilterClicked(checkedId: Int,
                            filterType: Int)
    }

}