package com.chillsweet.scbmobile.custom

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.provider.Settings
import com.chillsweet.scbmobile.R


/**
 * Created by chillsweet on 7/5/2017 AD.
 */
class DialogAlertCustom constructor(val activity: Activity) {

    fun alertNetWorkNotAvailable() {
        val dialog = AlertDialog.Builder(activity)
                .setCancelable(false)
                .setMessage(activity.getString(R.string.dialog_no_internet))
                .setPositiveButton(activity.getString(R.string.settings)) { _, _ ->
                    activity.startActivity(Intent(Settings.ACTION_SETTINGS))
                    activity.finish()
                }
                .setNegativeButton(activity.getString(R.string.cancel), { _, _ -> activity.finish() })
                .create()
        dialog.show()
    }

    fun alertMessage(title: String?, error: String, finish: Boolean) {
        val dialog = AlertDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(error)
                .setPositiveButton(activity.getString(R.string.close), { _, _ -> if (finish) activity.finish() })
                .create()
        dialog.show()
    }

    fun alertRequestNotSuccess(responseCode: Int, message: String) {
        val dialog = AlertDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(String.format("Code: %d", responseCode))
                .setMessage(message)
                .setPositiveButton(activity.getString(R.string.ok), { _, _ -> activity.finish() })
                .create()
        dialog.show()
    }

    fun alertConfirm(message: String, dialogOKClickListener: DialogInterface.OnClickListener) {
        val dialog = AlertDialog.Builder(activity)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton(activity.getString(R.string.cancel), { _, _ -> })
                .setPositiveButton(activity.getString(R.string.ok), dialogOKClickListener)
                .create()
        dialog.show()
    }
}