package com.chillsweet.scbmobile.custom

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.widget.ImageView
import android.widget.LinearLayout
import com.chillsweet.scbmobile.R


/**
 * Created by Untitled on 1/26/2017.
 */

class ViewPagerDotIndicatorCustom : LinearLayout {
    private var viewPager: ViewPager? = null
    private var pagerAdapter: PagerAdapter? = null
    private var pageCount: Int = 0

    private var imageView: ImageView? = null
    private var size = 4
    private var color = Color.WHITE
    private var drawShadow = true
    private var alpha = 127

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    /**
     * call this method after you have done customizing

     * @param viewPager
     */
    fun setViewPager(viewPager: ViewPager) {
        this.viewPager = viewPager
        pagerAdapter = viewPager.adapter
        //            if (pagerAdapter instanceof HighlightPagerAdapter) {
        //                this.pageCount = ((HighlightPagerAdapter) pagerAdapter).getRealCount();
        //            } else {
        this.pageCount = pagerAdapter!!.count
        //            }
//        if(this.pageCount > 1) {
//            drawIndicator(size, 0)
//            setViewPagerPageChangeListener()
//        }
        drawIndicator(size, 0)
        setViewPagerPageChangeListener()
    }

    private fun setViewPagerPageChangeListener() {
        viewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                //                    if (pagerAdapter instanceof HighlightPagerAdapter) {
                //                        drawIndicator(size, position % pageCount);
                //                    } else {
                drawIndicator(size, position)
                //                    }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private fun drawIndicator(radius: Int, position: Int) {
        try {
            if (imageView != null) removeView(imageView)
            val r = dpToPx(radius)
            val s = dpToPx(radius + 2)
            val x = dpToPx(radius - 2)
            val b = Bitmap.createBitmap(s * 2 * pageCount, s * 2, Bitmap.Config.ARGB_8888)
            val c = Canvas(b)
            for (i in 0..pageCount - 1) {
                val p = Paint(Paint.ANTI_ALIAS_FLAG)
                p.alpha = alpha
                if (drawShadow)
                    p.setShadowLayer(dpToPx(1).toFloat(), 0f, dpToPx(1).toFloat(), ContextCompat.getColor(context, R.color.gray))
                if (i == position) {
                    p.color = ContextCompat.getColor(context, R.color.colorPrimary)
                    c.drawCircle((s + i * s * 2).toFloat(), s.toFloat(), r.toFloat(), p)
                } else {
                    p.color = ContextCompat.getColor(context, R.color.gray)
                    c.drawCircle((s + i * s * 2).toFloat(), s.toFloat(), r.toFloat(), p)
                }
            }
            imageView = ImageView(context)
            imageView!!.setImageBitmap(b)
            addView(imageView)
        } catch (e: Exception) {
            if (e.message != null)
                Log.e("drawIndicator", e.message.toString())
        }

    }

    private fun dpToPx(dp: Int): Int {
        val r = context.resources
        val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics).toInt()
        return px
    }

    fun setAlpha(alpha: Int) {
        this.alpha = alpha
    }

    fun setSize(size: Int) {
        this.size = size
    }

    fun setColor(color: Int) {
        this.color = color
    }

    fun setDrawShadow(drawShadow: Boolean) {
        this.drawShadow = drawShadow
    }
}


