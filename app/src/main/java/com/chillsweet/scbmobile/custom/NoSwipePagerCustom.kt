package com.chillsweet.scbmobile.custom

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Created by chillsweet on 30/6/2018 AD
 */
class NoSwipePagerCustom(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var enabledSwipe = true

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (this.enabledSwipe) {
            super.onTouchEvent(event)
        } else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (this.enabledSwipe) {
            super.onInterceptTouchEvent(event)
        } else false
    }

    fun setPagingEnabled(enabled: Boolean) {
        this.enabledSwipe = enabled
    }
}