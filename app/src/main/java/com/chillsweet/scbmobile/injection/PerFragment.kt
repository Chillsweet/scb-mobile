package com.chillsweet.scbmobile.injection

import javax.inject.Scope

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Scope
@Retention
annotation class PerFragment