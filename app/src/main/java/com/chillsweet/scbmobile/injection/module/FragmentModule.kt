package com.chillsweet.scbmobile.injection.module

import android.app.Activity
import android.content.Context
import android.support.v4.app.Fragment
import com.chillsweet.scbmobile.injection.ActivityContext
import dagger.Module
import dagger.Provides

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Module
class FragmentModule(private val mFragment: Fragment) {

    @Provides
    internal fun provideFragment(): Fragment = mFragment

    @Provides
    internal fun provideActivity(): Activity = mFragment.activity!!

    @Provides
    @ActivityContext
    internal fun provideContext(): Context = mFragment.activity!!

}