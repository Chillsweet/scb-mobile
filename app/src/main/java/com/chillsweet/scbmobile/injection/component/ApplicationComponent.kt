package com.chillsweet.scbmobile.injection.component

import android.app.Application
import android.content.Context
import com.chillsweet.scbmobile.data.DataManager
import com.chillsweet.scbmobile.data.remote.Services
import com.chillsweet.scbmobile.data.sqlite.SqliteController
import com.chillsweet.scbmobile.injection.ApplicationContext
import com.chillsweet.scbmobile.injection.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Singleton
@Component(modules = [(ApplicationModule::class)])
interface ApplicationComponent {

    @ApplicationContext
    fun context(): Context

    fun application(): Application

    fun dataManager(): DataManager

    fun callServices(): Services

    fun callSqlite(): SqliteController
}