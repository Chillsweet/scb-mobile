package com.chillsweet.scbmobile.injection.component

import com.chillsweet.scbmobile.injection.ConfigPersistent
import com.chillsweet.scbmobile.injection.module.ActivityModule
import com.chillsweet.scbmobile.injection.module.FragmentModule
import dagger.Component

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@ConfigPersistent
@Component(dependencies = [(ApplicationComponent::class)])
interface ConfigPersistentComponent {

    fun activityComponent(activityModule: ActivityModule): ActivityComponent

    fun fragmentComponent(fragmentModule: FragmentModule): FragmentComponent

}