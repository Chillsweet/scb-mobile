package com.chillsweet.scbmobile.injection.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.chillsweet.scbmobile.data.remote.Services
import com.chillsweet.scbmobile.data.remote.ServicesFactory
import com.chillsweet.scbmobile.data.sqlite.SqliteController
import com.chillsweet.scbmobile.injection.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Module
class ApplicationModule(private val mApplication: Application) {

    @Provides
    internal fun provideApplication(): Application = mApplication

    @Provides
    @ApplicationContext
    internal fun provideContext(): Context = mApplication

    @Provides
    @Singleton
    internal fun provideSqlite(): SqliteController = SqliteController(provideContext())

    @Provides
    @Singleton
    internal fun provideServices(): Services = ServicesFactory.provideServices(provideContext())

}