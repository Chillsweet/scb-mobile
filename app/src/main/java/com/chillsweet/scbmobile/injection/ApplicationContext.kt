package com.chillsweet.scbmobile.injection

import javax.inject.Qualifier

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Qualifier
@Retention
annotation class ApplicationContext