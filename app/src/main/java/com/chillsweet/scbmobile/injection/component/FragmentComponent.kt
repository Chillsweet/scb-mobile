package com.chillsweet.scbmobile.injection.component

import com.chillsweet.scbmobile.injection.PerFragment
import com.chillsweet.scbmobile.injection.module.FragmentModule
import com.chillsweet.scbmobile.ui.list.favorite.FavoriteListFragment
import com.chillsweet.scbmobile.ui.list.mobile.MobileListFragment
import dagger.Subcomponent

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@PerFragment
@Subcomponent(modules = [(FragmentModule::class)])
interface FragmentComponent {

    fun inject(mobileListFragment: MobileListFragment)

    fun inject(favoriteListFragment: FavoriteListFragment)

}