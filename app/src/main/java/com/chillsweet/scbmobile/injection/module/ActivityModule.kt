package com.chillsweet.scbmobile.injection.module

import android.app.Activity
import android.content.Context
import com.chillsweet.scbmobile.injection.ActivityContext
import dagger.Module
import dagger.Provides

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@Module
class ActivityModule(private val mActivity: Activity) {

    @Provides
    internal fun provideActivity(): Activity = mActivity

    @Provides
    @ActivityContext
    internal fun provideContext(): Context = mActivity

}