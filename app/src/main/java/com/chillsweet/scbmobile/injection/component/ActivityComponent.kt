package com.chillsweet.scbmobile.injection.component

import com.chillsweet.scbmobile.injection.PerActivity
import com.chillsweet.scbmobile.injection.module.ActivityModule
import com.chillsweet.scbmobile.ui.base.BaseActivity
import com.chillsweet.scbmobile.ui.detail.MobileDetailActivity
import com.chillsweet.scbmobile.ui.main.MainActivity
import dagger.Subcomponent

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@PerActivity
@Subcomponent(modules = [(ActivityModule::class)])
interface ActivityComponent {

    fun inject(baseActivity: BaseActivity)

    fun inject(mainActivity: MainActivity)

    fun inject(mobileDetailActivity: MobileDetailActivity)

}