package com.chillsweet.scbmobile.ui.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by chillsweet on 30/6/2018 AD
 */
open class BaseContract<V : BaseMvpView> : BasePresenter<V> {

    var mvpView: V? = null
        private set
    private val mCompositeDisposable = CompositeDisposable()

    override fun attachView(mvpView: V) {
        this.mvpView = mvpView
    }

    override fun detachView() {
        mvpView = null
        if (!mCompositeDisposable.isDisposed) {
            mCompositeDisposable.clear()
        }
    }

    private val isViewAttached: Boolean
        get() = mvpView != null

    fun checkViewAttached() {
        if (!isViewAttached) throw MvpViewNoAttachedException()
    }

    fun addDisposable(dis: Disposable) {
        mCompositeDisposable.add(dis)
    }

    fun clearDisposable() {
        mCompositeDisposable.clear()
    }

    private class MvpViewNoAttachedException internal constructor() : RuntimeException("Please call Presenter.attachView(MvpView) before" + " requesting data to the Presenter")
}
