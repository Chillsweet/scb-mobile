package com.chillsweet.scbmobile.ui.list.mobile

import com.chillsweet.scbmobile.data.DataManager
import com.chillsweet.scbmobile.data.remote.ServiceResult
import com.chillsweet.scbmobile.injection.ConfigPersistent
import com.chillsweet.scbmobile.ui.base.BaseContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@ConfigPersistent
class MobileListPresenter @Inject
constructor(val mDataManager: DataManager) : BaseContract<MobileListView>() {

    override fun attachView(mvpView: MobileListView) {
        super.attachView(mvpView)
    }

    fun callMobileList() {
        mvpView?.showLoading()
        checkViewAttached()
        addDisposable(mDataManager.callMobileList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            mvpView?.hideLoading()
                            when(response.code()) {
                                in ServiceResult.Success -> {
                                    response.body()!!.forEach {
                                        if (mDataManager.mSqlite.isMobileFavoriteExist(it))
                                            mDataManager.mSqlite.updateMobileFavorite(it)
                                    }
                                    mvpView?.setMobileList(response.body()!!)
                                }
                                in ServiceResult.Error -> {
                                    mvpView?.showRequestNotSuccess(response.code())
                                }
                            }
                        },
                        { error ->
                            mvpView?.hideLoading()
                            mvpView?.showError(error)
                        }
                )
        )
    }

}