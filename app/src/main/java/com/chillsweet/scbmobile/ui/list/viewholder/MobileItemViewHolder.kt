package com.chillsweet.scbmobile.ui.list.viewholder

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.data.sqlite.SqliteController
import com.chillsweet.scbmobile.extension.setImageCenterCrop
import com.chillsweet.scbmobile.extension.setImageFitCenter
import kotlinx.android.synthetic.main.vh_mobile_item.view.*
import timber.log.Timber

/**
 * Created by chillsweet on 30/6/2018 AD
 */
class MobileItemViewHolder(val context: Context,
                           itemView: View)
    : RecyclerView.ViewHolder(itemView) {

    fun bindMobileList(mMobile: Mobile,
                       isFavorite: Boolean,
                       clickListener: ClickListener) {
        itemView.apply {
            imgMobile.setImageCenterCrop(mMobile.url)
            tvName.text = mMobile.name
            tvDesc.text = mMobile.description
            tvPrice.text = String.format("%s: \$%.2f", context.getString(R.string.price), mMobile.price)
            tvRating.text = String.format("%s: %.1f", context.getString(R.string.rating), mMobile.rating)

            if (isFavorite)
                btnFav.setImageResource(R.drawable.ic_fav_click)
            else
                btnFav.setImageResource(R.drawable.ic_fav)

            btnFav.setOnClickListener { clickListener.onFavIconClicked(mMobile, btnFav) }
        }

        itemView.setOnClickListener { clickListener.onItemClicked(mMobile) }
    }

    fun bindFavoriteList(mMobile: Mobile,
                         clickListener: ClickListener) {
        itemView.apply {
            imgMobile.setImageCenterCrop(mMobile.url)
            tvName.text = mMobile.name
            tvDesc.text = mMobile.description
            tvPrice.text = String.format("%s: %.1f", context.getString(R.string.rating), mMobile.rating)
            tvRating.visibility = View.GONE
            btnFav.visibility = View.GONE
        }
        itemView.setOnClickListener { clickListener.onItemClicked(mMobile) }
    }

    interface ClickListener {
        fun onItemClicked(mMobile: Mobile)

        fun onFavIconClicked(mMobile: Mobile,
                             btnFav: ImageButton)
    }

}