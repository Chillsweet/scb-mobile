package com.chillsweet.scbmobile.ui.list.mobile

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageButton
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.data.remote.ServiceResult
import com.chillsweet.scbmobile.ui.base.BaseFragment
import com.chillsweet.scbmobile.ui.detail.MobileDetailActivity
import com.chillsweet.scbmobile.ui.list.viewholder.MobileItemViewHolder
import com.chillsweet.scbmobile.ui.list.viewholder.MobileListAdapter
import com.chillsweet.scbmobile.util.Constance
import com.chillsweet.scbmobile.util.NetWorkUtils
import kotlinx.android.synthetic.main.view_recyclerview.*
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
class MobileListFragment : BaseFragment(), MobileListView,
        MobileItemViewHolder.ClickListener {

    @Inject
    lateinit var presenter: MobileListPresenter

    @Inject
    lateinit var adapter: MobileListAdapter

    private lateinit var mMobileList: ArrayList<Mobile>

    var mCallback: OnAddFavoriteListener? = null

    companion object {
        fun newInstance() = MobileListFragment().apply {
            arguments = Bundle().apply {

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent().inject(this)
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        bindUI()
        loadData()
    }

    override val layoutId: Int
        get() = R.layout.view_recyclerview

    override fun bindUI() {
    }

    fun setAdapter() {
        adapter.setAdapter(presenter.mDataManager.mSqlite, mMobileList, Constance.TYPE_MOBILE)
        adapter.setClickListener(this)
    }

    fun setRecyclerView() {
        rvItem.layoutManager = LinearLayoutManager(activity)
        rvItem.adapter = adapter
    }

    fun updateBtnFav() {
        adapter.updateAdapter(mMobileList)
    }

    fun updateSort(filterType: Int) {
        when (filterType) {
            Constance.FILTER_1 -> Collections.sort(mMobileList, Mobile.lowPrice)
            Constance.FILTER_2 -> Collections.sort(mMobileList, Mobile.highPrice)
            Constance.FILTER_3 -> Collections.sort(mMobileList, Mobile.rating51)
        }
        adapter.updateAdapter(mMobileList)
    }

    override fun loadData() {
        if (NetWorkUtils.isNetworkAvailable(activity, true))
            presenter.callMobileList()
    }

    override fun showLoading() {
        loadingView.showLoading()
    }

    override fun hideLoading() {
        loadingView.showContent()
    }

    override fun showError(error: Throwable) {
        Timber.e(error.message.toString())
        ServiceResult.onThrowableException(activity, error)
    }

    override fun showRequestNotSuccess(responseCode: Int) {
        ServiceResult.onAlertError(activity, responseCode, presenter.mDataManager)
    }

    override fun setMobileList(mMobileList: ArrayList<Mobile>) {
        this.mMobileList = mMobileList
        setAdapter()
        setRecyclerView()
    }

    override fun onItemClicked(mMobile: Mobile) {
        MobileDetailActivity.startActivity(activity, mMobile)
    }

    override fun onFavIconClicked(mMobile: Mobile,
                                  btnFav: ImageButton) {
        with(presenter.mDataManager.mSqlite) {
            when (isMobileFavoriteExist(mMobile)) {
                true  -> {
                    btnFav.setImageResource(R.drawable.ic_fav)
                    deleteMobileFavorite(mMobile)
                    mCallback?.onItemUnClickedFavorite(mMobile)
                }
                false -> {
                    btnFav.setImageResource(R.drawable.ic_fav_click)
                    saveMobileFavorite(mMobile)
                    mCallback?.onItemClickedFavorite(mMobile)
                }
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallback = activity as OnAddFavoriteListener
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    interface OnAddFavoriteListener {
        fun onItemClickedFavorite(mMobile: Mobile)

        fun onItemUnClickedFavorite(mMobile: Mobile)
    }
}