package com.chillsweet.scbmobile.ui.base

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.util.LongSparseArray
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.chillsweet.scbmobile.BaseApplication
import com.chillsweet.scbmobile.injection.component.ActivityComponent
import com.chillsweet.scbmobile.injection.component.ConfigPersistentComponent
import com.chillsweet.scbmobile.injection.component.DaggerConfigPersistentComponent
import com.chillsweet.scbmobile.injection.module.ActivityModule
import kotlinx.android.synthetic.main.view_toolbar.*
import timber.log.Timber
import java.util.concurrent.atomic.AtomicLong

/**
 * Created by chillsweet on 30/6/2018 AD
 */
abstract class BaseActivity : AppCompatActivity() {

    private var mActivityComponent: ActivityComponent? = null
    private var mActivityId: Long = 0

    companion object {
        private const val KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID"
        private val NEXT_ID = AtomicLong(0)
        private val sComponentsArray = LongSparseArray<ConfigPersistentComponent>()
    }

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(layoutId)

        mActivityId = savedInstanceState?.getLong(KEY_ACTIVITY_ID) ?: NEXT_ID.getAndIncrement()
        val configPersistentComponent: ConfigPersistentComponent
        if (sComponentsArray.get(mActivityId) == null) {
            Timber.i("Creating new ConfigPersistentComponent id=%d", mActivityId)
            configPersistentComponent = DaggerConfigPersistentComponent.builder()
                    .applicationComponent(BaseApplication[this].component)
                    .build()
            sComponentsArray.put(mActivityId, configPersistentComponent)
        }
        else {
            Timber.i("Reusing ConfigPersistentComponent id=%d", mActivityId)
            configPersistentComponent = sComponentsArray.get(mActivityId)
        }
        mActivityComponent = configPersistentComponent.activityComponent(ActivityModule(this))
        mActivityComponent?.inject(this)

    }

    abstract val layoutId: Int
    abstract fun bindUI()
    abstract fun loadData()

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_ACTIVITY_ID, mActivityId)
    }

    override fun onDestroy() {
        if (!isChangingConfigurations) {
            Timber.i("Clearing ConfigPersistentComponent id=%d", mActivityId)
            sComponentsArray.remove(mActivityId)
        }
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun activityComponent(): ActivityComponent = mActivityComponent as ActivityComponent

    protected fun setToolBar(toolBar: Toolbar, titleName: String) {
        setSupportActionBar(toolBar)
        supportActionBar!!.apply {
            setDisplayShowTitleEnabled(false)
            tvTitle?.apply {
                visibility = View.VISIBLE
                text = titleName
            }
        }
    }

    protected fun setToolBarHome(toolBar: Toolbar, titleName: String) {
        setSupportActionBar(toolBar)
        supportActionBar!!.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            tvTitle?.apply {
                visibility = View.VISIBLE
                text = titleName
            }
        }
    }

}