package com.chillsweet.scbmobile.ui.detail

import com.chillsweet.scbmobile.data.DataManager
import com.chillsweet.scbmobile.data.remote.ServiceResult
import com.chillsweet.scbmobile.injection.ConfigPersistent
import com.chillsweet.scbmobile.ui.base.BaseContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@ConfigPersistent
class MobileDetailPresenter @Inject
constructor(val mDataManager: DataManager) : BaseContract<MobileDetailView>() {

    override fun attachView(mvpView: MobileDetailView) {
        super.attachView(mvpView)
    }

    fun callMobileImageList(mobileId: Int) {
        mvpView?.showLoading()
        checkViewAttached()
        addDisposable(mDataManager.callMobileImageList(mobileId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            mvpView?.hideLoading()
                            when(response.code()) {
                                in ServiceResult.Success -> {
                                    mvpView?.setMobileDetail(response.body()!!)
                                }
                                in ServiceResult.Error   -> {
                                    mvpView?.showRequestNotSuccess(response.code())
                                }
                            }
                        },
                        { error ->
                            mvpView?.hideLoading()
                            mvpView?.showError(error)
                        }
                )
        )
    }

}