package com.chillsweet.scbmobile.ui.list.favorite

import com.chillsweet.scbmobile.data.DataManager
import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.injection.ConfigPersistent
import com.chillsweet.scbmobile.ui.base.BaseContract
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@ConfigPersistent
class FavoriteListPresenter @Inject
constructor(val mDataManager: DataManager) : BaseContract<FavoriteListView>() {

    override fun attachView(mvpView: FavoriteListView) {
        super.attachView(mvpView)
    }

    fun getMobileFavoriteList(): ArrayList<Mobile>? {
        return mDataManager.getMobileFavorite()
    }

}