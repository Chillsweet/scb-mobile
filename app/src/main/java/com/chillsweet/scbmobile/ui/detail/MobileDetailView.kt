package com.chillsweet.scbmobile.ui.detail

import com.chillsweet.scbmobile.data.model.MobileImage
import com.chillsweet.scbmobile.ui.base.BaseMvpView

/**
 * Created by chillsweet on 30/6/2018 AD
 */
interface MobileDetailView : BaseMvpView {

    fun setMobileDetail(mMobileImageList: List<MobileImage>)

}