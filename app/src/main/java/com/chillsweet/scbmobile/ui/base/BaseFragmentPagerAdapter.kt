package com.chillsweet.scbmobile.ui.base

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import javax.inject.Inject

/**
 * Created by chillsweet on 10/30/2017 AD.
 */
class BaseFragmentPagerAdapter @Inject
constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private var mFragmentTitleList = ArrayList<String>()
    private var mFragmentList = ArrayList<Fragment>()

    override fun getItem(position: Int): Fragment = mFragmentList[position]

    override fun getCount(): Int = mFragmentList.size

    fun addFrag(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence = mFragmentTitleList[position]
}