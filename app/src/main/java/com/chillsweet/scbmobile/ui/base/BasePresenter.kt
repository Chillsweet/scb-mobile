package com.chillsweet.scbmobile.ui.base

/**
 * Created by chillsweet on 30/6/2018 AD
 */
interface BasePresenter<in V : BaseMvpView> {

    fun attachView(mvpView: V)

    fun detachView()

}