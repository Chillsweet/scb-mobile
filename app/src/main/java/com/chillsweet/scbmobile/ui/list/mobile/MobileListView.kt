package com.chillsweet.scbmobile.ui.list.mobile

import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.ui.base.BaseMvpView

/**
 * Created by chillsweet on 30/6/2018 AD
 */
interface MobileListView : BaseMvpView{

    fun setMobileList(mMobileList: ArrayList<Mobile>)

}