package com.chillsweet.scbmobile.ui.list.favorite

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.ImageButton
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.ui.base.BaseFragment
import com.chillsweet.scbmobile.ui.detail.MobileDetailActivity
import com.chillsweet.scbmobile.ui.list.viewholder.MobileItemViewHolder
import com.chillsweet.scbmobile.ui.list.viewholder.MobileListAdapter
import com.chillsweet.scbmobile.util.Constance
import com.chillsweet.scbmobile.util.RecyclerItemTouchHelper
import kotlinx.android.synthetic.main.view_recyclerview.*
import timber.log.Timber
import java.util.*
import javax.inject.Inject


/**
 * Created by chillsweet on 30/6/2018 AD
 */
class FavoriteListFragment : BaseFragment(), FavoriteListView,
        MobileItemViewHolder.ClickListener,
        RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    @Inject
    lateinit var presenter: FavoriteListPresenter

    @Inject
    lateinit var adapter: MobileListAdapter

    private lateinit var mMobileList: ArrayList<Mobile>

    var mCallback: OnDeleteListener? = null

    companion object {
        fun newInstance() = FavoriteListFragment().apply {
            arguments = Bundle().apply {

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent().inject(this)
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        bindUI()
        loadData()
    }

    override val layoutId: Int
        get() = R.layout.view_recyclerview

    override fun bindUI() {
    }

    fun setAdapter() {
        adapter.setAdapter(presenter.mDataManager.mSqlite, mMobileList, Constance.TYPE_FAVORITE)
        adapter.setClickListener(this)
    }

    fun setRecyclerView() {
        rvItem.layoutManager = LinearLayoutManager(activity)
        rvItem.adapter = adapter

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.RIGHT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvItem)
    }

    fun updateFavoriteList() {
        mMobileList.clear()
        if (presenter.getMobileFavoriteList() != null)
            mMobileList.addAll(presenter.getMobileFavoriteList()!!)
        adapter.updateAdapter(mMobileList)
    }

    fun updateSort(filterType: Int) {
        when (filterType) {
            Constance.FILTER_1 -> Collections.sort(mMobileList, Mobile.lowPrice)
            Constance.FILTER_2 -> Collections.sort(mMobileList, Mobile.highPrice)
            Constance.FILTER_3 -> Collections.sort(mMobileList, Mobile.rating51)
        }
        adapter.updateAdapter(mMobileList)
    }

    override fun loadData() {
        mMobileList = ArrayList()
        presenter.getMobileFavoriteList()?.let {
            this@FavoriteListFragment.mMobileList = it
        }
        setAdapter()
        setRecyclerView()
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showError(error: Throwable) {
    }

    override fun showRequestNotSuccess(responseCode: Int) {
    }

    override fun onItemClicked(mMobile: Mobile) {
        MobileDetailActivity.startActivity(activity, mMobile)
    }

    override fun onFavIconClicked(mMobile: Mobile,
                                  btnFav: ImageButton) {
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder,
                          direction: Int,
                          position: Int) {
        if (viewHolder is MobileItemViewHolder) {
            presenter.mDataManager.mSqlite.deleteMobileFavorite(mMobileList[position])
            adapter.removeItem(position)
            mCallback?.onItemDeleted(mMobileList[position])
            mMobileList.removeAt(position)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallback = activity as OnDeleteListener
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    interface OnDeleteListener {
        fun onItemDeleted(mobile: Mobile)
    }
}