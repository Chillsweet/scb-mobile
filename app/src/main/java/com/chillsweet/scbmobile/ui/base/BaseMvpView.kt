package com.chillsweet.scbmobile.ui.base

/**
 * Created by chillsweet on 30/6/2018 AD
 */
interface BaseMvpView {

    fun showLoading()

    fun hideLoading()

    fun showError(error: Throwable)

    fun showRequestNotSuccess(responseCode: Int)

}