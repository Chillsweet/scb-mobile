package com.chillsweet.scbmobile.ui.list.viewholder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.data.sqlite.SqliteController
import com.chillsweet.scbmobile.util.Constance
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
class MobileListAdapter @Inject
constructor() : RecyclerView.Adapter<MobileItemViewHolder>() {

    private lateinit var sqliteController: SqliteController
    private lateinit var mMobileList: ArrayList<Mobile>

    private lateinit var clickListener: MobileItemViewHolder.ClickListener

    private var type = 0

    fun setAdapter(sqliteController: SqliteController,
                   mMobileList: ArrayList<Mobile>,
                   type: Int) {
        this.sqliteController = sqliteController
        this.mMobileList = mMobileList
        this.type = type
    }

    fun updateAdapter(mMobileList: ArrayList<Mobile>) {
        this.mMobileList = mMobileList
        notifyDataSetChanged()
    }

    fun setClickListener(clickListener: MobileItemViewHolder.ClickListener) {
        this.clickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MobileItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.vh_mobile_item, parent, false)
        return MobileItemViewHolder(parent.context, view)
    }

    override fun onBindViewHolder(holder: MobileItemViewHolder,
                                  position: Int) {
        when (type) {
            Constance.TYPE_MOBILE   -> holder.bindMobileList(mMobileList[position], sqliteController.isMobileFavoriteExist(mMobileList[position]), clickListener)
            Constance.TYPE_FAVORITE -> holder.bindFavoriteList(mMobileList[position], clickListener)
        }
    }

    override fun getItemCount(): Int = mMobileList.size

    fun removeItem(position: Int) {
        notifyItemRemoved(position)
    }
}