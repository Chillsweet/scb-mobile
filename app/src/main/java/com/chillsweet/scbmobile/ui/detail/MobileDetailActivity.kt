package com.chillsweet.scbmobile.ui.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.data.model.MobileImage
import com.chillsweet.scbmobile.data.remote.ServiceResult
import com.chillsweet.scbmobile.ui.base.BaseActivity
import com.chillsweet.scbmobile.util.NetWorkUtils
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.view_toolbar.*
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
class MobileDetailActivity : BaseActivity(), MobileDetailView {

    @Inject
    lateinit var presenter: MobileDetailPresenter

    private lateinit var adapter: ImageListAdapter

    private lateinit var mMobile: Mobile

    companion object {

        const val EXTRAS_MOBILE = "EXTRAS_MOBILE"

        fun startActivity(activity: Activity,
                          mMobile: Mobile) {
            val intent = Intent(activity, MobileDetailActivity::class.java).apply {
                putExtra(EXTRAS_MOBILE, mMobile)
            }
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        presenter.attachView(this)
        getIntentExtras()
        bindUI()
        loadData()
    }

    override val layoutId: Int
        get() = R.layout.activity_detail

    fun getIntentExtras() {
        intent?.extras?.apply {
            mMobile = getParcelable(EXTRAS_MOBILE)
        }
    }

    override fun bindUI() {
        setToolBarHome(toolBar, getString(R.string.app_name))
    }

    override fun loadData() {
        if (NetWorkUtils.isNetworkAvailable(this, true))
            presenter.callMobileImageList(mMobile.id)
    }

    override fun showLoading() {
        loadingView.showLoading()
    }

    override fun hideLoading() {
        loadingView.showContent()
    }

    override fun showError(error: Throwable) {
        Timber.e(error.message.toString())
        ServiceResult.onThrowableException(this, error)
    }

    override fun showRequestNotSuccess(responseCode: Int) {
        ServiceResult.onAlertError(this, responseCode, presenter.mDataManager)
    }

    override fun setMobileDetail(mMobileImageList: List<MobileImage>) {
        adapter = ImageListAdapter(this, mMobileImageList)
        vpImage.adapter = adapter
        indicator.setViewPager(vpImage)
        tvName.text = mMobile.name
        tvBrand.text = mMobile.brand
        tvDesc.text = mMobile.description
        tvPrice.text = String.format("%s: \$%.2f", getString(R.string.price), mMobile.price)
        tvRating.text = String.format("%s: %.1f", getString(R.string.rating), mMobile.rating)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}