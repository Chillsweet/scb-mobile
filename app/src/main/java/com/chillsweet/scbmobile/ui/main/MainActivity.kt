package com.chillsweet.scbmobile.ui.main

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.Menu
import android.view.MenuItem
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.custom.DialogFilterCustom
import com.chillsweet.scbmobile.data.model.Mobile
import com.chillsweet.scbmobile.ui.base.BaseActivity
import com.chillsweet.scbmobile.ui.base.BaseFragmentPagerAdapter
import com.chillsweet.scbmobile.ui.list.favorite.FavoriteListFragment
import com.chillsweet.scbmobile.ui.list.mobile.MobileListFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_toolbar.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView,
        MobileListFragment.OnAddFavoriteListener,
        FavoriteListFragment.OnDeleteListener,
        DialogFilterCustom.ClickListener {

    @Inject
    lateinit var presenter: MainPresenter

    private lateinit var adapter: BaseFragmentPagerAdapter

    private lateinit var fragmentMobile: MobileListFragment
    private lateinit var fragmentFavorite: FavoriteListFragment

    private var checkedId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        presenter.attachView(this)
        bindUI()
    }

    override val layoutId: Int
        get() = R.layout.activity_main

    override fun bindUI() {
        setToolBar(toolBar, getString(R.string.app_name))
        setAdapter()
        setViewPager()
        setTabLayout()
    }

    fun setAdapter() {
        fragmentMobile = MobileListFragment.newInstance()
        fragmentFavorite = FavoriteListFragment.newInstance()

        adapter = BaseFragmentPagerAdapter(supportFragmentManager)
        adapter.addFrag(fragmentMobile, getString(R.string.mobile_list))
        adapter.addFrag(fragmentFavorite, getString(R.string.favorite_list))
    }

    fun setViewPager() {
        vpItem.adapter = adapter
        vpItem.setPagingEnabled(false)
    }

    fun setTabLayout() {
        tabLayout.setupWithViewPager(vpItem)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                vpItem.currentItem = tab.position
            }
        })
    }

    override fun loadData() {
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showError(error: Throwable) {
    }

    override fun showRequestNotSuccess(responseCode: Int) {
    }

    override fun onItemClickedFavorite(mMobile: Mobile) {
        fragmentFavorite.updateFavoriteList()
    }

    override fun onItemUnClickedFavorite(mMobile: Mobile) {
        fragmentFavorite.updateFavoriteList()
    }

    override fun onItemDeleted(mobile: Mobile) {
        fragmentMobile.updateBtnFav()
    }

    override fun onFilterClicked(checkedId: Int,
                                 filterType: Int) {
        this.checkedId = checkedId
        fragmentMobile.updateSort(filterType)
        fragmentFavorite.updateSort(filterType)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menu_filter -> {
                DialogFilterCustom(this).showDialog(checkedId, this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
