package com.chillsweet.scbmobile.ui.detail

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import com.chillsweet.scbmobile.R
import com.chillsweet.scbmobile.data.model.MobileImage
import com.chillsweet.scbmobile.extension.setImageCenterCrop
import com.chillsweet.scbmobile.extension.setImageFitCenter
import kotlinx.android.synthetic.main.view_mobile_image.view.*
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
class ImageListAdapter @Inject
constructor(val context: Context, val mMobileImageList: List<MobileImage>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = mMobileImageList.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: View = View.inflate(context, R.layout.view_mobile_image, null)
        view.imgMobile.setImageFitCenter(mMobileImageList[position].url)
        view.setOnClickListener { }
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    interface ClickListener {
        fun onItemClicked()
    }

}