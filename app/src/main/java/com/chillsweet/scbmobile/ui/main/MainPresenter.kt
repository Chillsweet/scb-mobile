package com.chillsweet.scbmobile.ui.main

import com.chillsweet.scbmobile.data.DataManager
import com.chillsweet.scbmobile.injection.ConfigPersistent
import com.chillsweet.scbmobile.ui.base.BaseContract
import javax.inject.Inject

/**
 * Created by chillsweet on 30/6/2018 AD
 */
@ConfigPersistent
class MainPresenter @Inject
constructor(val mDataManager: DataManager) : BaseContract<MainView>(){

    override fun attachView(mvpView: MainView) {
        super.attachView(mvpView)
    }

}